var express = require('express');
var app = express();
var port = 8085;
var cheerio = require('cheerio');
var fs = require('fs');
var request = require('request');
var moment = require('moment');

var url = fs.readFileSync('./test.html');

var fileWithoutCRLF = url.toString().replace(/(?:\\[rn]|[\r\n]+)+/g, "");
var cleanFile = fileWithoutCRLF.replace(/\\/g, "");

request(url, function(err, resp, body){    
    
    function getItems(classOrId, firstLastOrAllFlag, extractNumberFlag, extractDateFlag ){
        moment.locale('fr');
        if(firstLastOrAllFlag === "first"){
            var res = $(classOrId).first();
            var resText = res.text(); 
        }
        else if(firstLastOrAllFlag === "last"){
            var res = $(classOrId).last();
            var resText = res.text();
        }
        else if(firstLastOrAllFlag === "all"){
            var resText = [];
            $(classOrId).map(function(i, el) {
                return resText.push($(this).text());
              })
        }
        else if(extractDateFlag){ 
            var res = $(classOrId);
            var resText = res.text(); 
            var regEx = /(\d{4}([.\-/ ])\d{2}\2\d{2}|\d{2}([.\-/ ])\d{2}\3\d{4})/g;
            var extractedDates = resText.match(regEx) + '';
            var splitedDates = extractedDates.split(',');
            var dates = [];
            for (var i = 0 ; i <= splitedDates.length - 1 ; i++ ){
                splitedDates[i] = moment(splitedDates[i], 'DD/MM/YYYY');
                singleDate = new Date(splitedDates[i]);
                dates.push(moment(singleDate).format());
            }            
            return dates;            
        }
        if(extractNumberFlag){
            resText = resText.match(/\d+/g).map(Number);
            resText = resText.toString().replace(/,/g, '.');
        }        
        return resText;
    }

    function removeMultipleSpacesAndTrimStrings(str){        
    
        str = str.trim();
        str = str.replace(/\s\s+/g, ' ');
        return str.split(' ');
    
    }

    function extractTextBetweenParentheses(arr){
        
        let array = [...new Set(arr)];
        for (var i = 0 ; i <= array.length - 1 ; i++){
            var regExp = /\(([^)]+)\)/;
            array[i] = regExp.exec(array[i])[1];
        }
        array = [...new Set(array)];
        return array;
    }

    function extractNumberAndSpliceTheRest(arr){
        var isNotNumberIndex = [];
        for (var i = 0; i<= arr.length-1; i++){
        var matchedNumberString = arr[i].match(/\d+/g);
        if(matchedNumberString === null){
            isNotNumberIndex.push(i);
        }else{
            var number = matchedNumberString.map(Number);            
            arr[i] = number.toString().replace(/,/g, '.');   
        }
    }
    
    for (var i = isNotNumberIndex.length -1; i >= 0; i--){
        arr.splice(isNotNumberIndex[i], 1);        
    }
    return arr;
    }

    var $ = cheerio.load(cleanFile);
    var code = getItems('.pnr-ref .pnr-info', 'last');
    var name = getItems('.pnr-name .pnr-info', 'last');
    var price = getItems('.very-important', 'last', true);
    var types = getItems('.travel-way','all');
    var dates = getItems('.pnr-summary', '', false, true);
    var arrivalTimes = getItems('.origin-destination-border.origin-destination-hour.segment-arrival','all');
    var departureTimes = getItems('.origin-destination-hour.segment-departure','all');
    var departureStations = getItems('.origin-destination-station.segment-departure', 'all');
    var arrivalStations = getItems('.origin-destination-border.origin-destination-station.segment-arrival', 'all');
    
    var meansOfTravel =  $('.origin-destination-station.segment-departure').next('.segment');
    var means = meansOfTravel.text();
    var numberOfTrip = $('.origin-destination-station.segment-departure').next('.segment').next('.segment');
    var numbers = numberOfTrip.text();
    var passengers =extractTextBetweenParentheses(getItems('.typology', 'all'));
    
        
    var splitedTrips = removeMultipleSpacesAndTrimStrings(means);    
    var splitedNumbers = removeMultipleSpacesAndTrimStrings(numbers);

    var singlePrices = getItems('.cell', 'all');

    singlePrices = extractNumberAndSpliceTheRest(singlePrices);
    

    var resultFile = {}
    resultFile.status = "ok";
    resultFile.result = {
        trips:[]
    };
    resultFile.result.trips = {
        'code': code,
        'name': name,
        details:{}
    }
    resultFile.result.custom = [];
    for (var i = 0 ; i <= singlePrices.length-1; i++){
        resultFile.result.custom.push({
            'value' : singlePrices[i]
        })
    }
    
    resultFile.result.trips.details = {
        'price': price,
        roundTrips: []
    }
    for(var t = 0; t <= dates.length-1; t++){
        if(t== dates.length-1){
            resultFile.result.trips.details.roundTrips.push({
                'type': types[t],
                'date': dates[t],
                trains: [
                    {
                        'departureTime': departureTimes[t],
                        'departureStation': departureStations[t],
                        'arrivalTime': arrivalTimes[t],
                        'arrivalStations': arrivalStations[t],
                        'type': splitedTrips[t],
                        'number': splitedNumbers[t],
                        passengers: [
                            { 
                        "type": "échangeable",
                        "age": "(26 à 59 ans)"
                    }
                ]                      

                    }
                ]
            });    
        }else{

            resultFile.result.trips.details.roundTrips.push({
                'type': types[t],
                'date': dates[t],
                trains: [
                {
                    'departureTime': departureTimes[t],
                    'departureStation': departureStations[t],
                    'arrivalTime': arrivalTimes[t],
                    'arrivalStations': arrivalStations[t],
                    'type': splitedTrips[t],
                    'number': splitedNumbers[t]
                }
            ]
        });
    }
    }
    
    var json = JSON.stringify(resultFile);
    fs.writeFile('test-result.json', json, { flag: 'wx' }, function (err) {
        if (err) {
            fs.readFile('test-result.json', 'utf8', function readFileCallback(err, data){
                if (err){
                    console.log(err);
                } else {
                var json = JSON.stringify(resultFile);
                fs.writeFile('test-result.json', json, 'utf8'); 
            }
        });
            
        };
        console.log("It's saved!");
    });
    

});



app.listen(port);
console.log("running on " + port );
